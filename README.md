**DEPRECATED** This package is depracated since *infopack* now uses .writeFile() to output files

# infopack-gen-copy-files

A infopack generator.

## Usage

`dest`-property can be set on a globel level (`settings.dest`) or on instruction level (`settings.instructions[0].dest`) or defaults to **output_files**.

All paths are given relative to working folder,

```
gen.step(settings)
```

**Example**
```
gen.step({
    readFrom: 'some/path/relative/to/working-folder',
    dest: 'some/path/relative/to/working-folder',
    instructions: [{
        readFrom: 'some/path/relative/to/working-folder',
        pattern: 'path or glob',
        dest: 'some/path/relative/to/working-folder'
})
```